
class Line:
    """
    Stores information about individual rows.

    'rowtype' holds the current estimate while 'score' holds the accompanying
    measure of goodness of fit.
    """

    def __init__(self,line):
        self.raw = line
        self.rowtype = 'unknown'
        self.score = 1.0 # high score associated with rowtype

        # attributes for records
        self.letter = 'unknown'
        self.fname = 'unknown'
        self.lname = 'unknown'
        self.sex = 'unknown'
        self.age = 'unknown'
        self.occupation = 'unknown'
        self.streetnumber = 'unknown'
        self.prev_address = 'unknown'

        # attributes for street
        self.streetname = 'unknown'

        # attributes for group number
        self.groupnum = 'unknown'


    """
    Define simple feature recognition indicator functions
    """

    def is_new_page(line):
        return ( line[0] == '+++ NEWPAGE +++' )

    def is_blank_line(line):
        return ( line[0] == '+++ BLANK LINE +++' )

    def has_leading_upper_letter(line):
        return bool( re.match("^[A-Z]$",line[0]) )

    def has_leading_lower_letter(line):
        return bool( re.match("^[a-z]$",line[0]) )
        
    def is_num_group(line):
        return bool( re.match("^[0-9]{4,5}$",line[0]) and len(line)==1 )

    def is_street_name(line):
        return bool( re.match("^[a-zA-Z]{2,30}$",line[0]) and
                     re.match("^[a-zA-Z]{2,30}$",line[1]) and
                     len(line)==2 ) 

    # MORE TO COME...
