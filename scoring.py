
class Scoring:
    """
    Defines the scoring rules (or a probability measure), which map features to
    rowtype scores.

    Simple scoring rules instead of probabilities (for now):
    Think of score as analogous to a likelihood ratio.
    Each rowtype will start with a prior score of 1.
    Evidence in favor of a particular rowtype will amplify its score (and vice
    versa).
    After all the evidence (features) has been processed, the rowtype with the
    highest score wins, and is assigned to the line object.
    """

    # UNDER CONSTRUCTION

