# A probabilistic approach

I want to classify rows by analyzing the tokens.
This would involve defining a set of features of tokens and rows, and a probability measure to judge the accuracy of the classification.
A simple approach would be to classify each row independently.
However, this would be ignoring a wealth of information from the structure of the data.

A full probabilistic model would assign a joint probability to the rows and their tokens.
This would yield a first best solution, but it suffers the curse of dimensionality -- enriching the set of features would cause the computational burden to explode. 

A more computationally efficient approach would be to use a multi-stage algorithm.
The initial stage would naively classify each row independently.
The subsequent stages would then use the previous stage classification to provide information about neighboring rows to further refine the classification.
This would essentially be augmenting the first stage classifier with additional information from nearby rows. 
This would be repeated several times, until some convergence criterion is met.

## Features

Row types and corresponding token types by column:
- headers
- noise
- street name
    - [4-5 digit numerical] two words
- groupings (a row that shows up each time the letter counter resets)
    - one number 4-5 digits
- records (the actual data)
    - Letter: ordered A-Z, with grouping rows separating
    - Veteran
    - Last name
    - First name
    - Initial
    - Sex
    - Address
    - Occupation
    - Age
    - Previous year's address
