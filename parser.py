"""
Instructions: run the script with one command line argument for the data directory.
For example:
> python parser.py ocrdata/
"""

import sys
import os
import glob
import re
from line import Line
from scoring import Scoring

def read_ocr_data(data_dir):
    """
    Read in the data and store each line in a Line object.
    Reads all txt files in data_dir.
    Return an array of lines in sorted order.
    """

    # build list of filenames
    ocrfiles = sorted(glob.glob(os.path.join(data_dir,'*.txt')))
    # load all of the ocr text into a list of line objects
    ocrlines = []
    for filename in ocrfiles:
        # mark pages
        ocrlines.append( Line( ['+++ NEWPAGE +++',os.path.basename(filename)] ) )
        # files aren't encoded in the standard utf-8
        with open(filename, encoding='latin-1') as ocrfile:
            for line in ocrfile:
                # each line is split on whitespace and stored in a list
                candidate = line.split()
                # store in Line object
                if len(candidate) > 0:
                    ocrlines.append(Line(candidate))
                else:
                    ocrlines.append( Line(['+++ BLANK LINE +++',
                                           os.path.basename(filename)]) )
    return ocrlines

def row_parser():
    """
    Categorizes rows based on scores.
    If scoring takes into account neighboring rows, this function can be called
    repeatedly, to refine the classification until convergence.
    """
    # parsing routine under construction

if __name__ == '__main__':
    ocrlines = read_ocr_data(sys.argv[1])
